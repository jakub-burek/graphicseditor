#include "CFigure.h"

CFigure::CFigure(unsigned int ID) : IdNumber(ID)
{

}

unsigned int CFigure::GetIdNumber()
{
	return IdNumber;
}

void CFigure::ResetIdNumberAfterCopying(unsigned int ID)
{
	IdNumber = ID;
}

void CFigure::DrawFigure(sf::RenderWindow& renderWindow)
{
	OnDrawFigure(renderWindow);

	for (const auto& figurePoint : FigurePoints)
	{
		if (figurePoint)
			figurePoint->OnFrame(renderWindow);
	}
}

void CFigure::SetPosition(const sf::Vector2f & NewPosition)
{
	GlobalCenterPositon = NewPosition;
}

const sf::Vector2f & CFigure::GetPosition()
{
	return GlobalCenterPositon;
}

void CFigure::OnDrawFigure(sf::RenderWindow&)
{
	//To override.
}

IFigurePoint* CFigure::AddFigurePoint(const sf::Vector2f& startPosition)
{
	auto figurePoint = IFigurePoint::Create(startPosition, *this);
	if (!figurePoint)
		return nullptr;

	const auto result = figurePoint.get();

	FigurePoints.emplace_back(std::move(figurePoint));
	return result;
}

void CFigure::RotateFigurePoints(float rotationDegrees)
{
	for (const auto& figurePoint : FigurePoints)
	{
		if (figurePoint)
			figurePoint->Rotate(rotationDegrees);
	}
}