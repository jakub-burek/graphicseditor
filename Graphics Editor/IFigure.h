#pragma once
#if !defined(__I_Figure__)
#define __I_Figure__

#include <SFML/Graphics.hpp>

class IFigure {
public:
	IFigure() = default;
	virtual ~IFigure() = default;

	virtual void SetPosition(const sf::Vector2f&) = 0;
	virtual const sf::Vector2f& GetPosition() = 0;
	virtual void SetColor(const sf::Color&) = 0;
	virtual IFigure* ReturnFigureCopy() = 0;
	virtual void DrawFigure(sf::RenderWindow&) = 0;
	virtual bool CheckIfMarked(const sf::Vector2f&) = 0;
	virtual void ResizeFigure(const int &) = 0;
	virtual unsigned int GetIdNumber() = 0;
	virtual void ResetIdNumberAfterCopying(unsigned int ID) = 0;
	virtual void RotateFigure(const int& delta) = 0;
	virtual std::string GetDataString() = 0;
};

#endif //__I_Figure__