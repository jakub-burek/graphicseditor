#include"DuplicatorTool.h"

DuplicatorTool::DuplicatorTool(const unsigned int stackMaxCapacity)
{

}

void DuplicatorTool::GetFigureToDuplicatorToolStackAfterCreation(std::shared_ptr<IFigure> DataHandlerPointer)
{
	UndoFiguresStack.push_back(DuplicatorToolVectorElement(DataHandlerPointer, true));
}

void DuplicatorTool::GetFigureToDuplicatorToolStackBeforeEdition(std::shared_ptr<IFigure> DataHandlerPointer)
{
	UndoFiguresStack.push_back(DuplicatorToolVectorElement(DataHandlerPointer, false));
}

void DuplicatorTool::UndoAction(FigureHandler & CurrentFigureHandler)
{
	if (UndoFiguresStack.begin() != UndoFiguresStack.end())
	{
		auto element = --UndoFiguresStack.end();
		CurrentFigureHandler.AlterFigure(element->GetHandleToCurrentVersion(), element->GetOldFigureCopy());
		RedoFiguresStack.push_back(*element);
		UndoFiguresStack.pop_back();
	}
}

void DuplicatorTool::RepeatAction(FigureHandler & CurrentFigureHandler)
{
	if (RedoFiguresStack.begin() != RedoFiguresStack.end())
	{
		auto element = --RedoFiguresStack.end();
		CurrentFigureHandler.AlterFigure(element->GetHandleToCurrentVersion(), element->GetOldFigureCopy());
		UndoFiguresStack.push_back(*element);
		RedoFiguresStack.pop_back();
	}
}

bool DuplicatorTool::IdNumberExists(unsigned int ID)
{
	for (auto element : UndoFiguresStack)
		if (element.GetHandleToCurrentVersion() != nullptr && (element.GetHandleToCurrentVersion())->GetIdNumber() == ID)
			return true;
	for (auto element : RedoFiguresStack)
		if (element.GetHandleToCurrentVersion() != nullptr && (element.GetHandleToCurrentVersion())->GetIdNumber() == ID)
			return true;
	return false;
}

void DuplicatorTool::ClearDuplicatorToolStack()
{
	UndoFiguresStack.clear();
	RedoFiguresStack.clear();
}
