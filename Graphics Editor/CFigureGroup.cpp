#include "CFigureGroup.h"

CFigureGroup::CFigureGroup(unsigned int ID, std::vector<std::shared_ptr<IFigure>>& SetOfFigures)
{
	GroupOfFigures = SetOfFigures;
	IdNumber = ID;
}

unsigned int CFigureGroup::GetIdNumber()
{
	return IdNumber;
}

void CFigureGroup::ResetIdNumberAfterCopying(unsigned int ID)
{
	IdNumber = ID;
}

void CFigureGroup::DrawFigure(sf::RenderWindow & window)
{
	for (const auto& element : GroupOfFigures)
		element->DrawFigure(window);
}

void CFigureGroup::SetPosition(const sf::Vector2f & NewPosition)
{
	auto Offset = NewPosition - (*GroupOfFigures.begin())->GetPosition();

	for (auto& element : GroupOfFigures)
		element->SetPosition(element->GetPosition() + Offset);
}

const sf::Vector2f & CFigureGroup::GetPosition()
{
	return (*GroupOfFigures.begin())->GetPosition();
}
