#include"MainWindowView.h"

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

int main()
{
	srand((unsigned int)time(NULL));
	MainWindowView MainWindow(sf::VideoMode(1000, 800), "Graphics Editor", sf::Style::Default);
	MainWindow.RunMainWindowView();
}