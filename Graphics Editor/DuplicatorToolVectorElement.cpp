#include"DuplicatorToolVectorElement.h"

DuplicatorToolVectorElement::DuplicatorToolVectorElement(std::shared_ptr<IFigure> DataHandlerPointer, bool Creation)
{
	if (Creation == true)
	{
		OldFigureCopy = nullptr;
		HandleToCurrentVersion = DataHandlerPointer;
	}
	else
	{
		HandleToCurrentVersion = DataHandlerPointer;
		if (DataHandlerPointer != nullptr)
			OldFigureCopy = std::shared_ptr<IFigure>(DataHandlerPointer->ReturnFigureCopy());
		else
			OldFigureCopy = nullptr;
	}
}

std::shared_ptr<IFigure> DuplicatorToolVectorElement::GetHandleToCurrentVersion()
{
	return HandleToCurrentVersion;
}

IFigure* DuplicatorToolVectorElement::GetOldFigureCopy()
{
	if (OldFigureCopy != nullptr)
		return OldFigureCopy->ReturnFigureCopy();
	else
		return nullptr;
}