#pragma once
#if !defined(__Line__)
#define __Line__

#include"CFigure.h"

class Line : public CFigure {
public:
	Line(unsigned int ID, const sf::Vector2f& BeginPositionVector, const sf::Vector2f& EndPositionVector, const sf::Color& color);
	virtual void SetColor(const sf::Color& color)  override final;
	virtual IFigure* ReturnFigureCopy() override final;
	virtual bool CheckIfMarked(const sf::Vector2f& Position) override final;
	virtual void ResizeFigure(const int & delta) override final;
	virtual void RotateFigure(const int& delta) override final;
	virtual std::string GetDataString() override final;

protected:
	virtual void OnDrawFigure(sf::RenderWindow& window) override final;

private:
	sf::Vertex line[2];
	sf::Color  lineColor;
	IFigurePoint* BeginPosition;
	IFigurePoint* EndPosition;
};

#endif //__Line__