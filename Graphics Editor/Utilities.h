#pragma once
#if !defined(__Utilities__)
#define __Utilities__

#include <random>
#include "FigureHandler.h"
#include "DuplicatorTool.h"

class DuplicatorTool;
class FigureHandler;

# define PI 3.141592653589793238462643383279502884L
unsigned int IDGenerator(FigureHandler& CurrentFigureHandler, DuplicatorTool& CurrentDuplicatorTool);
sf::Vector2f RotatePoint(sf::Vector2f center, float radians, sf::Vector2f currentPosition);
sf::Vector2f CalculateVectorAB(const sf::Vector2f& A, const sf::Vector2f& B);

#endif //__Utilities__