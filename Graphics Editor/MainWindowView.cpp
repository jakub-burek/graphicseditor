#include"MainWindowView.h"

MainWindowView::MainWindowView(sf::VideoMode videoMode, sf::String windowName, int windowStyleEnum) :
	MainWindow(videoMode, windowName, windowStyleEnum)
{
	for (auto& element : BackgruoundColorTable)
		element = 0.f;
	for (auto& element : FigureColorTable)
		element = 0.f;
}

bool MainWindowView::RunMainWindowView()
{
	//MainWindow Settings
	//MainWindow.setVerticalSyncEnabled(true);
	MainWindow.setTitle("Graphic Editor");
	MainWindow.setFramerateLimit(100);
	MainWindow.resetGLStates();

	//Setting MainWindowFlags
	MainWindowFlags = 0;
	MainWindowFlags |= ImGuiWindowFlags_MenuBar;
	MainWindowFlags |= ImGuiWindowFlags_NoScrollbar;
	MenuIsHovered = false;
	bool HelpIsCalled = false;

	//Setting up data structures
	FigureHandler FigureHandler;
	Clipboard     Clipboard;
	DuplicatorTool DuplicatorTool(10);
	char fileNameBuffer[255] = { 0 };

	ImGui::SFML::Init(MainWindow);

	//Main Program Loop
	while (MainWindow.isOpen())
	{
		ImGui::SFML::Update(MainWindow, MainClock.restart());

		//Event Processing Loop
		sf::Event event;
		while (MainWindow.pollEvent(event))
		{
			ImGui::SFML::ProcessEvent(event);
			if (HelpIsCalled)
				continue;

			if (event.type == sf::Event::Closed)
				MainWindow.close();
			if (!MenuIsHovered)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) && event.type == sf::Event::MouseWheelMoved)
				{
					Clipboard.SynchroniseWithDuplicatorToolBeforeEdition(DuplicatorTool);
					Clipboard.RotateFigure(event.mouseWheel.delta);
				}
				else if (event.type == sf::Event::MouseWheelMoved)
				{
					Clipboard.SynchroniseWithDuplicatorToolBeforeEdition(DuplicatorTool);
					Clipboard.ResizeFigure(event.mouseWheel.delta);
				}

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					Clipboard.AddFigureToClipboardOrClear(FigureHandler.GetFigureToClipBoard((sf::Vector2f)sf::Mouse::getPosition(MainWindow)));
				}

				static auto wasDragging = false;
				static auto processDrag = false;

				if (!wasDragging && ImGui::IsMouseDragging(1))
					processDrag = (FigureHandler.GetFigureToClipBoard((sf::Vector2f)sf::Mouse::getPosition(MainWindow)) != nullptr);

				if (ImGui::IsMouseDragging(1) && processDrag)
				{
					if (!wasDragging)
						Clipboard.SynchroniseWithDuplicatorToolBeforeEdition(DuplicatorTool);

					Clipboard.SetPosition((sf::Vector2f)sf::Mouse::getPosition(MainWindow));
					wasDragging = true;
				}
				else
				{
					wasDragging = false;
					processDrag = false;
				}
			}
		}


		//Begining Window
		if (!ImGui::Begin("Graphic Editor", NULL, MainWindowFlags))
		{
			ImGui::End();
			return true;
		}

		if (ImGui::IsMouseHoveringAnyWindow())
			MenuIsHovered = true;
		else
			MenuIsHovered = false;

		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu("Menu"))
			{
				ImGui::MenuItem("File", NULL, false, false);
				if (ImGui::MenuItem("New"))
				{
					Clipboard.ClearClipboarFigureVector();
					DuplicatorTool.ClearDuplicatorToolStack();
					FigureHandler.ClearFigureVector();
				}
				if (ImGui::MenuItem("Open"))
				{
					Clipboard.ClearClipboarFigureVector();
					DuplicatorTool.ClearDuplicatorToolStack();
					FigureHandler.ReadDataFromFile(fileNameBuffer);
				}
				if (ImGui::MenuItem("Save"))
					FigureHandler.WriteDataToFile(fileNameBuffer);
				ImGui::Separator();
				if (ImGui::MenuItem("Help"))
				{
					HelpIsCalled = true;
				}
				if (ImGui::MenuItem("Quit"))
				{
					ImGui::End();
					return true;
				}
				ImGui::EndMenu();
			}
			ImGui::EndMenuBar();
		}

		//File Name Input
		ImGui::InputText("File Name", fileNameBuffer, sizeof(fileNameBuffer));
		
		//Color Edition Tools
		ImGuiColorEditor("Background color", BackgroundColor, BackgruoundColorTable);
		ImGuiColorEditor("Figure Color", FigureColor, FigureColorTable);

		//Add Figure Buttons
		if (ImGui::Button("Rectangle"))
		{
			FigureHandler.AddFigureAndSynchroniseWithDuplicatorTool(new Rectangle(IDGenerator(FigureHandler, DuplicatorTool), sf::Vector2f(0, 0), sf::Vector2f(100, 300), sf::Color::Red), DuplicatorTool);
		}
		ImGui::SameLine();
		if (ImGui::Button("Square"))
		{
			FigureHandler.AddFigureAndSynchroniseWithDuplicatorTool(new Square(IDGenerator(FigureHandler, DuplicatorTool), sf::Vector2f(0, 0), 100, sf::Color::Blue, 0), DuplicatorTool);
		}
		ImGui::SameLine();
		if (ImGui::Button("Circle"))
		{
			FigureHandler.AddFigureAndSynchroniseWithDuplicatorTool(new Circle(IDGenerator(FigureHandler, DuplicatorTool), sf::Vector2f(0, 0), 100, sf::Color::Green), DuplicatorTool);
		}
		ImGui::SameLine();
		if (ImGui::Button("Triangle"))
		{
			FigureHandler.AddFigureAndSynchroniseWithDuplicatorTool(new Triangle(IDGenerator(FigureHandler, DuplicatorTool), sf::Vector2f(0, 0),sf::Vector2f(0, 0), sf::Vector2f(100, 0), sf::Vector2f(0, 100), sf::Color::Yellow), DuplicatorTool);
		}
		ImGui::SameLine();
		if (ImGui::Button("Line"))
		{
			FigureHandler.AddFigureAndSynchroniseWithDuplicatorTool(new Line(IDGenerator(FigureHandler, DuplicatorTool), sf::Vector2f(0, 0), sf::Vector2f(100, 100), sf::Color::Magenta), DuplicatorTool);
		}
		ImGui::SameLine();
		if (ImGui::Button("Quadrangle"))
		{
			FigureHandler.AddFigureAndSynchroniseWithDuplicatorTool(new Quadrangle(IDGenerator(FigureHandler, DuplicatorTool), sf::Vector2f(0, 0), sf::Vector2f(0 , 0), sf::Vector2f(0, 500), sf::Vector2f(200, 500), sf::Vector2f(200, 0), sf::Color::Cyan), DuplicatorTool);
		}

		//Toolbar Buttons
		if (ImGui::Button("Undo"))
		{
			DuplicatorTool.UndoAction(FigureHandler);
		}
		ImGui::SameLine();
		if (ImGui::Button("Repeat"))
		{
			DuplicatorTool.RepeatAction(FigureHandler);
		}
		ImGui::SameLine();
		if (ImGui::Button("Group"))
		{
			Clipboard.SynchroniseWithDuplicatorToolBeforeEdition(DuplicatorTool);
			Clipboard.CreateGroup(FigureHandler, DuplicatorTool);
		}
		ImGui::SameLine();
		if (ImGui::Button("Ungroup"))
		{
			Clipboard.SynchroniseWithDuplicatorToolBeforeEdition(DuplicatorTool);
			Clipboard.DestroyGroup(FigureHandler, DuplicatorTool);
		}
		ImGui::SameLine();
		if (ImGui::Button("Copy"))
		{
			Clipboard.CopyFigure(FigureHandler, DuplicatorTool);
		}
		ImGui::SameLine();
		if (ImGui::Button("Delete"))
		{
			Clipboard.SynchroniseWithDuplicatorToolBeforeEdition(DuplicatorTool);
			Clipboard.DestroyFigure(FigureHandler);
		}
		ImGui::SameLine();
		if (ImGui::Button("Color"))
		{
			Clipboard.SynchroniseWithDuplicatorToolBeforeEdition(DuplicatorTool);
			Clipboard.SetColor(FigureColor);
		}
		ImGui::End();
		//Ending Window

		if (HelpIsCalled)
		{
			ImGui::OpenPopup("Help Window");
		}
		if (ImGui::BeginPopupModal("Help Window"))
		{
			ImGui::Text("Jakub Burek Programming Computers Project.\n");
			ImGui::Text("Program is a non-profit educational project.\n");
			ImGui::Text("Program uses SFML and ImGui libraries.\n");
			ImGui::Text("For help go to manual.\n");
			ImGui::Text("Gliwice 2018\n");
			ImGui::Separator();
			if (ImGui::Button("OK", ImVec2(120, 0)))
			{
				ImGui::CloseCurrentPopup(); 
				HelpIsCalled = false;
			}
			ImGui::EndPopup();
		}

		
		//Setting Background Color
		MainWindow.clear(BackgroundColor);
		//################
		FigureHandler.DrawFigures(MainWindow);
		//################
		ImGui::SFML::Render(MainWindow);
		MainWindow.display();
	}
	ImGui::SFML::Shutdown();
	return true;
}



void MainWindowView::ImGuiColorEditor(const char* Label, sf::Color& Color, float* ColorTable)
{
	if (ImGui::ColorEdit3(Label, ColorTable))
	{
		Color.r = static_cast<sf::Uint8>(ColorTable[0] * 255.f);
		Color.g = static_cast<sf::Uint8>(ColorTable[1] * 255.f);
		Color.b = static_cast<sf::Uint8>(ColorTable[2] * 255.f);
	}
}