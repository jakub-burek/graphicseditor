#pragma once
#if !defined(__CFigure_Group__)
#define __CFigure_Group__

#include "IFigure.h"
#include "Utilities.h"
#include <vector>
#include <memory>

class CFigureGroup : public IFigure {
public:
	CFigureGroup(unsigned int ID, std::vector<std::shared_ptr<IFigure>>& SetOfFigures);
	~CFigureGroup() = default;

	unsigned int GetIdNumber() override final;
	void ResetIdNumberAfterCopying(unsigned int ID) override final;
	virtual void DrawFigure(sf::RenderWindow& window) override final;
	virtual void SetPosition(const sf::Vector2f& NewPosition) override final;
	virtual const sf::Vector2f& GetPosition() override final;

protected:
	unsigned int IdNumber;
	std::vector<std::shared_ptr<IFigure>> GroupOfFigures;
};

#endif //__CFigure_Group__