#pragma once
#if !defined(__Square__)
#define __Square__

#include"Rectangle.h"

class Square : public CFigure {
public:
	Square(unsigned int ID, const sf::Vector2f& Position, const float& Lenght, const sf::Color& color, const float& rotation);
	Square(Square& CopiedSquare);

	virtual void SetColor(const sf::Color& color) override final;
	virtual IFigure* ReturnFigureCopy() override final;
	virtual bool CheckIfMarked(const sf::Vector2f& ClickedPosition) override final;
	virtual void ResizeFigure(const int & delta) override final;
	void RotateFigure(const int& delta) override final;
	virtual std::string GetDataString() override final;

protected:
	virtual void OnDrawFigure(sf::RenderWindow& window) override final;

private:
	sf::RectangleShape square;
};

#endif //__Square__