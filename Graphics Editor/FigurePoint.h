#pragma once
#if !defined(__Figure_Point__)
#define __Figure_Point__

#include <SFML/Graphics.hpp>
#include "IFigure.h"

#include <memory>

class IFigurePoint {
public:
	virtual ~IFigurePoint() = default;

	virtual sf::Vector2f CalculateAbsolutePosition() const = 0;
	virtual void Rotate(float) = 0;
	virtual const sf::Vector2f& GetRelativePosition() const = 0;
	virtual void OnFrame(sf::RenderWindow& renderWindow) = 0;

	static std::unique_ptr<IFigurePoint> Create(const sf::Vector2f& startPostition, IFigure& Owner);
};

#endif //__Figure_Point__