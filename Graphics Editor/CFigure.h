#pragma once
#if !defined(__C_Figure__)
#define __C_Figure__

#include "FigurePoint.h"
#include "IFigure.h"
#include <vector>

class CFigure: public IFigure {
public:
	CFigure(unsigned int ID);
	~CFigure() = default;

	unsigned int GetIdNumber() override final;
	void ResetIdNumberAfterCopying(unsigned int ID) override final;
	virtual void DrawFigure(sf::RenderWindow&) override final;
	virtual void SetPosition(const sf::Vector2f& NewPosition) override final;
	virtual const sf::Vector2f& GetPosition() override final;

protected:
	virtual void OnDrawFigure(sf::RenderWindow&);
	IFigurePoint* AddFigurePoint(const sf::Vector2f& startPosition);
	void RotateFigurePoints(float rotationDegrees);

protected:
	unsigned int IdNumber;
	std::vector<std::unique_ptr<IFigurePoint>> FigurePoints;
	sf::Vector2f GlobalCenterPositon;
};

#endif //__C_Figure__