#include "Circle.h"

Circle::Circle(unsigned int ID, const sf::Vector2f & Position, const float & radius, const sf::Color & color): CFigure(ID)
{
	SetPosition(Position);
	circle.setOrigin(sf::Vector2f(radius, radius));
	circle.setPosition(Position.x - radius, Position.y - radius);
	circle.setRadius(radius);
	circle.setFillColor(color);
}

Circle::Circle(Circle & CopiedCircle) : CFigure(CopiedCircle.GetIdNumber())
{
	SetPosition(CopiedCircle.GetPosition());
	circle = CopiedCircle.circle;
}

void Circle::SetColor(const sf::Color & color)
{
	circle.setFillColor(color);
}

IFigure * Circle::ReturnFigureCopy()
{
	return new Circle(*this);
}

bool Circle::CheckIfMarked(const sf::Vector2f & Position)
{
	if (pow(Position.x - GetPosition().x, 2) + pow(Position.y - GetPosition().y, 2) <= pow(circle.getRadius(), 2))
		return true;
	else
		return false;
}

void Circle::ResizeFigure(const int & delta)
{
	circle.setRadius(circle.getRadius() + delta * 5);
	circle.setOrigin(sf::Vector2f(circle.getRadius(), circle.getRadius()));

}

void Circle::RotateFigure(const int& delta)
{
	circle.rotate((float)delta * (float)1);
}

std::string Circle::GetDataString()
{
	return std::to_string(GetIdNumber()) + " " + std::to_string(GetPosition().x) + " " + std::to_string(GetPosition().y) + " " + std::to_string(circle.getRadius()) + " " + std::to_string(circle.getFillColor().r) + " " + std::to_string(circle.getFillColor().g) + " " + std::to_string(circle.getFillColor().b) + " " + std::to_string(circle.getFillColor().a) + " ";
}

void Circle::OnDrawFigure(sf::RenderWindow & window)
{
	circle.setPosition(GetPosition().x, GetPosition().y);
	window.draw(circle);
}
