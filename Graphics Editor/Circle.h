#pragma once
#if !defined(__Circle__)
#define __Circle__

#include "CFigure.h"

class Circle : public CFigure {
public:
	Circle(unsigned int ID, const sf::Vector2f& Position, const float& radius, const sf::Color& color);
	Circle(Circle& CopiedCircle);

	virtual void SetColor(const sf::Color& color) override final;
	virtual IFigure* ReturnFigureCopy() override final;
	virtual bool CheckIfMarked(const sf::Vector2f& Position) override final;
	virtual void ResizeFigure(const int & delta) override final;
	virtual void RotateFigure(const int& delta) override final;
	virtual std::string GetDataString() override final;

protected:
	virtual void OnDrawFigure(sf::RenderWindow& window) override final;

private:
	sf::CircleShape circle;
};

#endif //__Circle__