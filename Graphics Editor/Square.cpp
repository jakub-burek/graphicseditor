#include "Square.h"

Square::Square(unsigned int ID, const sf::Vector2f & Position, const float & Lenght, const sf::Color & color, const float& rotation) : CFigure(ID)
{
	SetPosition(Position);
	square.setPosition(Position.x + 0.5f *Lenght, Position.y + 0.5f *Lenght);
	square.setSize(sf::Vector2f(Lenght, Lenght));
	square.setOrigin(Lenght*0.5f, Lenght*0.5f);
	square.setFillColor(color);
	square.setRotation(rotation);
}

Square::Square(Square & CopiedSquare) : CFigure(CopiedSquare.GetIdNumber())
{
	SetPosition(CopiedSquare.GetPosition());
	square = CopiedSquare.square;
}

void Square::SetColor(const sf::Color & color)
{
	square.setFillColor(color);
}

IFigure * Square::ReturnFigureCopy()
{
	return new Square(*this);
}

bool Square::CheckIfMarked(const sf::Vector2f & ClickedPosition)
{
	auto globalBounds = square.getGlobalBounds();
	if (ClickedPosition.x >= globalBounds.left && ClickedPosition.x <= globalBounds.left + globalBounds.width && ClickedPosition.y >= globalBounds.top && ClickedPosition.y <= globalBounds.top + globalBounds.height)
		return true;
	else
		return false;

}

void Square::ResizeFigure(const int & delta)
{
	square.setSize(sf::Vector2f(square.getSize().x + delta * 5, square.getSize().y + delta * 5));
	square.setOrigin(square.getSize().x * 0.5f, square.getSize().x * 0.5f);
}

void Square::RotateFigure(const int & delta)
{
	square.rotate((float)delta);
}

std::string Square::GetDataString()
{
	return std::to_string(GetIdNumber()) + " " + std::to_string(GetPosition().x) + " " + std::to_string(GetPosition().y) + " " + std::to_string(square.getSize().x) + " " + std::to_string(square.getFillColor().r) + " " + std::to_string(square.getFillColor().g) + " " + std::to_string(square.getFillColor().b) + " " + std::to_string(square.getFillColor().a) + " " + std::to_string(square.getRotation()) + " ";
}

void Square::OnDrawFigure(sf::RenderWindow & window)
{
	square.setPosition(GetPosition().x, GetPosition().y);
	window.draw(square);
}
