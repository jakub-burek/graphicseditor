#pragma once
#if !defined(__Quadrangle__)
#define __Quadrangle__

#include"CFigure.h"

class Quadrangle : public CFigure {
public:
	Quadrangle(unsigned int ID, const sf::Vector2f& GlobalPosition, const sf::Vector2f& Vector0, const sf::Vector2f& Vector1, const sf::Vector2f& Vector2, const sf::Vector2f& Vector3, const sf::Color& color);
	
	virtual void SetColor(const sf::Color& color) override final;
	virtual IFigure* ReturnFigureCopy() override final;
	virtual bool CheckIfMarked(const sf::Vector2f& ClickedPosition) override final;
	virtual void ResizeFigure(const int & delta) override final;
	void RotateFigure(const int& delta) override final;
	virtual std::string GetDataString() override final;

protected:
	virtual void OnDrawFigure(sf::RenderWindow& window) override final;

private:
	sf::ConvexShape quadrangle;
	IFigurePoint* leftTopHelper;
	IFigurePoint* rightTopHelper;
	IFigurePoint* rightBottomHelper;
	IFigurePoint* leftBottomHelper;
};

#endif //__Quadrangle__