#include "FigureGroup.h"

FigureGroup::FigureGroup(unsigned int ID, std::vector<std::shared_ptr<IFigure>>& SetOfFigures) : CFigureGroup(ID, SetOfFigures)
{

}

void FigureGroup::SetColor(const sf::Color & color)
{
	for (auto& element : GroupOfFigures)
		element->SetColor(color);
}

IFigure * FigureGroup::ReturnFigureCopy()
{
	std::vector<std::shared_ptr<IFigure>> NewVector;
	for (auto& element : GroupOfFigures)
		NewVector.push_back(std::shared_ptr<IFigure>(element->ReturnFigureCopy()));
	return new FigureGroup(IdNumber, NewVector);
}

bool FigureGroup::CheckIfMarked(const sf::Vector2f & Position)
{
	for (const auto& element : GroupOfFigures)
		if (element->CheckIfMarked(Position))
			return true;
	return false;
}

void FigureGroup::ResizeFigure(const int & delta)
{
	for (auto& element : GroupOfFigures)
		element->ResizeFigure(delta);
}

void FigureGroup::RotateFigure(const int & delta)
{
	for (auto& element : GroupOfFigures)
		element->RotateFigure(delta);
}

std::string FigureGroup::GetDataString()
{
	std::string data = std::to_string(GetIdNumber()) + " " + std::to_string(GroupOfFigures.size()) + " \n";
	for (const auto& element : GroupOfFigures)
	{
		std::string helper(typeid(*element).name());
		data += helper + "\n";
		data += element->GetDataString() + "\n";
	}
	return data;
}

std::vector<std::shared_ptr<IFigure>> FigureGroup::ReturnGroupFigureVector()
{
	return GroupOfFigures;
}
