#pragma once
#if !defined(__Main_Window_View__)
#define __Main_Window_View__

#include "C:/Users/jakub/Desktop/imgui-master/imgui.h"
#include "C:/Users/jakub/Desktop/imgui-sfml-master/imgui-sfml.h"

#include <SFML/Graphics.hpp>

#include <iostream>
#include <string>

#include "FigureGroup.h"
#include "FigureHandler.h"
#include "Clipboard.h"
#include "Utilities.h"

class MainWindowView {
public:
	MainWindowView(sf::VideoMode videoMode, sf::String windowName, int windowStyleEnum);
	bool MainWindowView::RunMainWindowView();

private:
	void MainWindowView::ImGuiColorEditor(const char* Label, sf::Color& Color, float* ColorTable);
	sf::RenderWindow MainWindow;
	sf::Color BackgroundColor;
	float BackgruoundColorTable[3];
	sf::Color FigureColor;
	float FigureColorTable[3];
	ImGuiWindowFlags MainWindowFlags;
	sf::Clock MainClock;
	bool MenuIsHovered;
};

#endif //__Main_Window_View__