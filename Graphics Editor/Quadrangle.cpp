#include "Quadrangle.h"

Quadrangle::Quadrangle(unsigned int ID, const sf::Vector2f& GlobalPosition, const sf::Vector2f& Vector0, const sf::Vector2f& Vector1, const sf::Vector2f& Vector2, const sf::Vector2f& Vector3, const sf::Color& color) : CFigure(ID)
{
	SetPosition(GlobalPosition);
	quadrangle.setPointCount(4);
	quadrangle.setOrigin(sf::Vector2f((Vector0.x + Vector1.x + Vector2.x + Vector3.x) / 4, (Vector0.y + Vector1.y + Vector2.y + Vector3.y) / 4));
	quadrangle.setPosition(GlobalPosition - quadrangle.getOrigin());
	quadrangle.setFillColor(color);

	leftTopHelper = AddFigurePoint(Vector0 - quadrangle.getOrigin());
	rightTopHelper = AddFigurePoint(Vector1 - quadrangle.getOrigin());
	rightBottomHelper = AddFigurePoint(Vector2 - quadrangle.getOrigin());
	leftBottomHelper = AddFigurePoint(Vector3 - quadrangle.getOrigin());
}

void Quadrangle::SetColor(const sf::Color & color)
{
	quadrangle.setFillColor(color);
}

IFigure * Quadrangle::ReturnFigureCopy()
{
	return new Quadrangle(GetIdNumber(), GetPosition(), leftTopHelper->GetRelativePosition(), rightTopHelper->GetRelativePosition(), rightBottomHelper->GetRelativePosition(), leftBottomHelper->GetRelativePosition(), quadrangle.getFillColor());
}

bool Quadrangle::CheckIfMarked(const sf::Vector2f & ClickedPosition)
{
	auto globalBounds = quadrangle.getGlobalBounds();
	if (ClickedPosition.x >= globalBounds.left && ClickedPosition.x <= globalBounds.left + globalBounds.width && ClickedPosition.y >= globalBounds.top && ClickedPosition.y <= globalBounds.top + globalBounds.height)
		return true;
	else
		return false;
}

void Quadrangle::ResizeFigure(const int & delta)
{
	;//Not necessery
}

void Quadrangle::RotateFigure(const int & delta)
{
	RotateFigurePoints((float)delta);
}

std::string Quadrangle::GetDataString()
{
	return std::to_string(GetIdNumber()) + " " + std::to_string(GetPosition().x) + " " + std::to_string(GetPosition().y) + " " +
		   std::to_string(leftTopHelper->GetRelativePosition().x) + " " + std::to_string(leftTopHelper->GetRelativePosition().y) + " " +
		   std::to_string(rightTopHelper->GetRelativePosition().x) + " " + std::to_string(rightTopHelper->GetRelativePosition().y) + " " +
		   std::to_string(rightBottomHelper->GetRelativePosition().x) + " " + std::to_string(rightBottomHelper->GetRelativePosition().y) + " " +
		   std::to_string(leftBottomHelper->GetRelativePosition().x) + " " + std::to_string(leftBottomHelper->GetRelativePosition().y) + " " +
		   std::to_string(quadrangle.getFillColor().r) + " " + std::to_string(quadrangle.getFillColor().g) + " " + std::to_string(quadrangle.getFillColor().b) + " " + std::to_string(quadrangle.getFillColor().a) + " ";
}

void Quadrangle::OnDrawFigure(sf::RenderWindow & window)
{
	const auto leftTopPosition = leftTopHelper->CalculateAbsolutePosition();
	const auto rightTopPosition = rightTopHelper->CalculateAbsolutePosition();
	const auto rightBottomPosition = rightBottomHelper->CalculateAbsolutePosition();
	const auto leftBottomPosition = leftBottomHelper->CalculateAbsolutePosition();

	quadrangle.setOrigin(sf::Vector2f((leftTopHelper->GetRelativePosition().x + rightTopHelper->GetRelativePosition().x + rightBottomHelper->GetRelativePosition().x + leftBottomHelper->GetRelativePosition().x) / 4, (leftTopHelper->GetRelativePosition().y + rightTopHelper->GetRelativePosition().y + rightBottomHelper->GetRelativePosition().y + leftBottomHelper->GetRelativePosition().y) / 4));
	quadrangle.setPosition(GetPosition());
	quadrangle.setPoint(0, leftTopHelper->GetRelativePosition() + quadrangle.getOrigin());
	quadrangle.setPoint(1, rightTopHelper->GetRelativePosition() + quadrangle.getOrigin());
	quadrangle.setPoint(2, rightBottomHelper->GetRelativePosition() + quadrangle.getOrigin());
	quadrangle.setPoint(3, leftBottomHelper->GetRelativePosition() + quadrangle.getOrigin());

	window.draw(quadrangle);
}
