#include "FigurePoint.h"

#include "IFigure.h"
#include "imgui.h"
#include "Utilities.h"
#include <functional>

class CFigurePoint final : public IFigurePoint {
public:
	CFigurePoint(const sf::Vector2f& startPosition, IFigure& Owner);
	virtual ~CFigurePoint() override final = default;

	virtual sf::Vector2f CalculateAbsolutePosition() const override final;
	virtual void Rotate(float) override final;
	virtual const sf::Vector2f& GetRelativePosition() const override final;
	virtual void OnFrame(sf::RenderWindow& renderWindow) override final; 

private:
	bool IsColliding(const sf::Vector2f& position) const;
	void HandleDrag(sf::RenderWindow&);
	bool AcceptDrag(sf::RenderWindow&);

private:
	sf::CircleShape circleShape;
	bool isDragging = false;
	sf::Vector2f lastDragPosition;
	IFigure& ownerFigure;
	sf::Vector2f relativePosition;
	bool wasMousePressed = false;
};

CFigurePoint::CFigurePoint(const sf::Vector2f& startPosition, IFigure& Owner): ownerFigure(Owner), relativePosition(startPosition)
{
	circleShape.setRadius(8.0f);
	circleShape.setOutlineThickness(2.0f);
	circleShape.setOutlineColor(sf::Color::Black);
	circleShape.setFillColor(sf::Color::White);
}

sf::Vector2f CFigurePoint::CalculateAbsolutePosition() const
{
	return ownerFigure.GetPosition() + relativePosition;
}

const sf::Vector2f& CFigurePoint::GetRelativePosition() const
{
	return relativePosition;
}

void CFigurePoint::OnFrame(sf::RenderWindow& renderWindow)
{
	renderWindow.draw(circleShape);
	HandleDrag(renderWindow);

	const auto radius = circleShape.getRadius();
	circleShape.setPosition(CalculateAbsolutePosition() - sf::Vector2f{ radius, radius });
}

bool CFigurePoint::IsColliding(const sf::Vector2f& position) const
{
	const auto center = CalculateAbsolutePosition();
	const auto xDifference = center.x - position.x;
	const auto yDifference = center.y - position.y;
	const auto pointsDistance = sqrt(xDifference * xDifference + yDifference * yDifference);

	return pointsDistance <= circleShape.getRadius();
}

void CFigurePoint::HandleDrag(sf::RenderWindow& window)
{
	if (!AcceptDrag(window))
		return;

	const auto mousePosition = sf::Mouse::getPosition(window);
	const auto dragOffset = sf::Vector2f((float)mousePosition.x, (float)mousePosition.y) - lastDragPosition;

	relativePosition += dragOffset;
	lastDragPosition = sf::Vector2f((float)mousePosition.x, (float)mousePosition.y);
}

class CScopedGuard final
{
public:
	CScopedGuard(std::function<void()>&& callback) : m_Callback(std::move(callback)) {}
	~CScopedGuard()
	{
		if (m_Callback)
			m_Callback();
	}

private:
	const std::function<void()> m_Callback;
};

bool CFigurePoint::AcceptDrag(sf::RenderWindow& window)
{
	const auto isMousePressed = sf::Mouse::isButtonPressed(sf::Mouse::Left);

	CScopedGuard mouseReleaseGuard{ [this, isMousePressed]()
	{
		wasMousePressed = isMousePressed;
	} };

	if (!isMousePressed)
		return (isDragging = false);

	if (isDragging)
		return true;

	if (wasMousePressed)
		return false;

	wasMousePressed = isMousePressed;

	const auto mousePosition = sf::Mouse::getPosition(window);

	if ((isDragging = IsColliding(sf::Vector2f((float)mousePosition.x, (float)mousePosition.y))))
	{
		lastDragPosition = sf::Vector2f((float)mousePosition.x, (float)mousePosition.y);
		return true;
	}

	return false;
}

std::unique_ptr<IFigurePoint> IFigurePoint::Create(const sf::Vector2f& startPosition, IFigure& Owner)
{
	return std::make_unique<CFigurePoint>(startPosition, Owner);
}

void CFigurePoint::Rotate(float rotationDegree)
{
	const auto rotationRadians = rotationDegree * PI / 180.0f;
	const auto sinValue = sin(rotationRadians);
	const auto cosValue = cos(rotationRadians);

	relativePosition.x = relativePosition.x * (float)cosValue - relativePosition.y * (float)sinValue;
	relativePosition.y = relativePosition.x * (float)sinValue + relativePosition.y * (float)cosValue;
}