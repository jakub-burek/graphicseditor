#include "Line.h"

Line::Line(unsigned int ID, const sf::Vector2f & BeginPositionVector, const sf::Vector2f & EndPositionVector, const sf::Color & color) : CFigure(ID)
{
	SetPosition((BeginPositionVector + EndPositionVector) / 2.0f);
	BeginPosition = AddFigurePoint(BeginPositionVector - GetPosition());
	EndPosition = AddFigurePoint(EndPositionVector - GetPosition());
	lineColor = color;
}

void Line::SetColor(const sf::Color & color)
{
	lineColor = color;
}

IFigure * Line::ReturnFigureCopy()
{
	return new Line(GetIdNumber(), BeginPosition->CalculateAbsolutePosition(), EndPosition->CalculateAbsolutePosition(), lineColor);
}

bool Line::CheckIfMarked(const sf::Vector2f & Position)
{
	const auto FirstPoint = BeginPosition->CalculateAbsolutePosition();
	const auto SecondPoint = EndPosition->CalculateAbsolutePosition();

	if (FirstPoint.x == SecondPoint.y)
		return (abs(FirstPoint.x - Position.x) <= 5);
	else
		return(abs(Position.y + (SecondPoint.y - FirstPoint.y) / (SecondPoint.x - FirstPoint.x)*(FirstPoint.x - Position.x) - FirstPoint.y) <= 5);
}

void Line::ResizeFigure(const int & delta)
{
	;//Not necessery
}

void Line::RotateFigure(const int & delta)
{
	;//Not necessery
}

std::string Line::GetDataString()
{
	return std::to_string(GetIdNumber()) + " " + std::to_string(BeginPosition->CalculateAbsolutePosition().x) + " " + std::to_string(BeginPosition->CalculateAbsolutePosition().y) + " " + std::to_string(EndPosition->CalculateAbsolutePosition().x) + " " + std::to_string(EndPosition->CalculateAbsolutePosition().y) + " " + std::to_string(line[0].color.r) + " " + std::to_string(line[0].color.g) + " " + std::to_string(line[0].color.b) + " " + std::to_string(line[0].color.a) + " ";
}

void Line::OnDrawFigure(sf::RenderWindow & window)
{
	line->position = GetPosition();
	line[0] = BeginPosition->CalculateAbsolutePosition();
	line[1] = EndPosition->CalculateAbsolutePosition();
	line[0].color = lineColor;
	line[1].color = lineColor;
	window.draw(line, 2, sf::Lines);
}
