#pragma once
#if !defined(__DuplicatorTool__)
#define __DuplicatorTool__

#include"IFigure.h"
#include"FigureHandler.h"
#include"DuplicatorToolVectorElement.h"

#include<memory>

class FigureHandler;

class DuplicatorTool {
public:
	DuplicatorTool(const unsigned int stackMaxCapacity);
	~DuplicatorTool() = default;

	void GetFigureToDuplicatorToolStackAfterCreation(std::shared_ptr<IFigure> DataHandlerPointer);
	void GetFigureToDuplicatorToolStackBeforeEdition(std::shared_ptr<IFigure> DataHandlerPointer);
	void UndoAction(FigureHandler& CurrentFigureHandler);
	void RepeatAction(FigureHandler& CurrentFigureHandler);
	bool IdNumberExists(unsigned int ID);
	void ClearDuplicatorToolStack();

private:
	std::vector<DuplicatorToolVectorElement> UndoFiguresStack;
	std::vector<DuplicatorToolVectorElement> RedoFiguresStack;
};

#endif //__DuplicatorTool__