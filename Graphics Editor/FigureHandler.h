#pragma once
#if !defined(__FigureHandler__)
#define __FigureHandler__

#include"IFigure.h"
#include"DuplicatorTool.h"

#include "Rectangle.h"
#include "Square.h"
#include "Circle.h"
#include "Triangle.h"
#include "Line.h"
#include "Quadrangle.h"

#include<algorithm>
#include<vector>
#include<memory>
#include<fstream>

class DuplicatorTool;

class FigureHandler {
public:
	void AddFigureAndSynchroniseWithDuplicatorTool(IFigure* newFigure, DuplicatorTool& DuplicatorTool);
	std::shared_ptr<IFigure> GetFigureToClipBoard(const sf::Vector2f& Position);
	void AlterFigure(std::shared_ptr<IFigure> FigureHandle, IFigure* AlteringFigure);
	void DestroyFigure(std::shared_ptr<IFigure> destroyedFigure);
	void DrawFigures(sf::RenderWindow& window);
	bool IdNumberExists(unsigned int ID);
	void ReadDataFromFile(const std::string FileName);
	void WriteDataToFile(const std::string FileName);
	void ClearFigureVector();

private:
	IFigure* AddCircleFromFile(std::ifstream& InputFileStram);
	IFigure* AddLineFromFile(std::ifstream& InputFileStram);
	IFigure* AddQuadrangleFromFile(std::ifstream& InputFileStram);
	IFigure* AddRectangleFromFile(std::ifstream& InputFileStram);
	IFigure* AddSquareFromFile(std::ifstream& InputFileStram);
	IFigure* AddTriangleFromFile(std::ifstream& InputFileStram);
	bool ReadColorFromFile(std::ifstream & InputFileStram, sf::Color& color);
	bool ReadVectorFromFile(std::ifstream & InputFileStream, sf::Vector2f& Vector);

private:
	std::vector<std::shared_ptr<IFigure>> FigureVector;
};

#endif //__FigureHandler__