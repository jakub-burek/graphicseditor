#include "Clipboard.h"

void Clipboard::AddFigureToClipboardOrClear(std::shared_ptr<IFigure>& addedFigure)
{
	if (addedFigure == nullptr)
		ClipboardFigureVector.clear();
	else
		ClipboardFigureVector.push_back(addedFigure);
}

void Clipboard::SetPosition(const sf::Vector2f & newPosition)
{
	for (const auto& element : ClipboardFigureVector)
		element->SetPosition(newPosition);
}

void Clipboard::MoveFigures(const sf::Vector2f & offset)
{
	for (const auto& element : ClipboardFigureVector)
		element->SetPosition(element->GetPosition() + offset);
}

void Clipboard::SetColor(const sf::Color & color)
{
	for (const auto& element : ClipboardFigureVector)
		element->SetColor(color);
}

void Clipboard::ResizeFigure(const int & delta)
{
	for (const auto& element : ClipboardFigureVector)
		element->ResizeFigure(delta);
}

void Clipboard::DestroyFigure(FigureHandler & EditedFigureHandler)
{
	for (auto& element : ClipboardFigureVector)
		EditedFigureHandler.DestroyFigure(element);
	ClipboardFigureVector.clear();
}

void Clipboard::CopyFigure(FigureHandler & EditedFigureHandler, DuplicatorTool & DuplicatorTool)
{
	for (auto& element : ClipboardFigureVector)
	{
			IFigure * temporary = element->ReturnFigureCopy();
			temporary->ResetIdNumberAfterCopying(IDGenerator(EditedFigureHandler, DuplicatorTool));
			EditedFigureHandler.AddFigureAndSynchroniseWithDuplicatorTool(temporary, DuplicatorTool);
	}
}

void Clipboard::SynchroniseWithDuplicatorToolBeforeEdition(DuplicatorTool & DuplicatorTool)
{
	for (auto& element : ClipboardFigureVector)
		DuplicatorTool.GetFigureToDuplicatorToolStackBeforeEdition(element);
}

void Clipboard::RotateFigure(const int& delta)
{
	for (auto& element : ClipboardFigureVector)
		element->RotateFigure(delta);
}

void Clipboard::CreateGroup(FigureHandler & EditedFigureHandler, DuplicatorTool & DuplicatorTool)
{
	IFigure* NewGroup = new FigureGroup(IDGenerator(EditedFigureHandler, DuplicatorTool), ClipboardFigureVector);
	for (auto& element : ClipboardFigureVector)
		EditedFigureHandler.DestroyFigure(element);
	ClipboardFigureVector.clear();
	EditedFigureHandler.AddFigureAndSynchroniseWithDuplicatorTool(NewGroup, DuplicatorTool);
}

void Clipboard::DestroyGroup(FigureHandler & EditedFigureHandler, DuplicatorTool & DuplicatorTool)
{
	for (auto& element : ClipboardFigureVector)
	{
		FigureGroup* elementPointerCast = dynamic_cast<FigureGroup*>(element->ReturnFigureCopy());
		if (elementPointerCast)
		{
			auto GroupFigureVector = elementPointerCast->ReturnGroupFigureVector();
			EditedFigureHandler.DestroyFigure(element);
			for(auto& element: GroupFigureVector)
				EditedFigureHandler.AddFigureAndSynchroniseWithDuplicatorTool(element->ReturnFigureCopy(), DuplicatorTool);
		}
	}
	ClipboardFigureVector.clear();
}

void Clipboard::ClearClipboarFigureVector()
{
	ClipboardFigureVector.clear();
}
