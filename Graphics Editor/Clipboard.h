#pragma once
#if !defined(__Clipboard__)
#define __Clipboard__

#include"IFigure.h"
#include"DuplicatorTool.h"
#include"FigureGroup.h"
#include"Utilities.h"

#include<vector>
#include<memory>

class Clipboard {
public:
	void AddFigureToClipboardOrClear(std::shared_ptr<IFigure>& addedFigure);
	void SetPosition(const sf::Vector2f& newPosition);
	void MoveFigures(const sf::Vector2f& offset);
	void SetColor(const sf::Color& color);
	void ResizeFigure(const int &delta);
	void DestroyFigure(FigureHandler& EditedFigureHandler);
	void CopyFigure(FigureHandler& EditedFigureHandler, DuplicatorTool& DuplicatorTool);
	void SynchroniseWithDuplicatorToolBeforeEdition(DuplicatorTool& DuplicatorTool);
	void RotateFigure(const int& delta);
	void CreateGroup(FigureHandler& EditedFigureHandler, DuplicatorTool& DuplicatorTool);
	void DestroyGroup(FigureHandler& EditedFigureHandler, DuplicatorTool& DuplicatorTool);
	void ClearClipboarFigureVector();

private:
	std::vector<std::shared_ptr<IFigure>> ClipboardFigureVector;
};

#endif //__Clipboard__