#pragma once
#if !defined(__Triangle__)
#define __Triangle__

#include"CFigure.h"

class Triangle : public CFigure {
public:
	Triangle(unsigned int ID, const sf::Vector2f& GlobalPosition, const sf::Vector2f& Position, const sf::Vector2f& Vector1, const sf::Vector2f& Vector2, const sf::Color& color);

	virtual void SetColor(const sf::Color& color) override final;
	virtual IFigure* ReturnFigureCopy() override final;
	virtual bool CheckIfMarked(const sf::Vector2f& Position) override final;
	virtual void ResizeFigure(const int & delta) override final;
	virtual void RotateFigure(const int& delta) override final;
	virtual std::string GetDataString() override final;

protected:
	virtual void OnDrawFigure(sf::RenderWindow& window) override final;

private:
	sf::ConvexShape triangle;
	IFigurePoint* FirstHelper;
	IFigurePoint* SecondHelper;
	IFigurePoint* ThirdHelper;
};

#endif //__Triangle__