#pragma once
#if !defined(__Rectangle__)
#define __Rectangle__

#include<iostream>

#include"CFigure.h"
#include"Utilities.h"

class Rectangle : public CFigure {
public:
	Rectangle(unsigned int ID, const sf::Vector2f& Position, const sf::Vector2f& Lenght, const sf::Color& color);

	virtual void SetColor(const sf::Color& color) override final;
	virtual IFigure* ReturnFigureCopy() override final;
	virtual bool CheckIfMarked(const sf::Vector2f& ClickedPosition) override final;
	virtual void ResizeFigure(const int & delta) override final;
	void RotateFigure(const int& delta) override final;
	virtual std::string GetDataString() override final;

protected:
	virtual void OnDrawFigure(sf::RenderWindow& window) override final;

private:
	sf::ConvexShape rectangle;
	IFigurePoint* leftTopHelper;
	IFigurePoint* rightBottomHelper;
};

#endif //__Rectangle__