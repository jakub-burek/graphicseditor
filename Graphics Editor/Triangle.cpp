#include "Triangle.h"

Triangle::Triangle(unsigned int ID, const sf::Vector2f& GlobalPosition, const sf::Vector2f & Position0, const sf::Vector2f & Position1, const sf::Vector2f & Position2, const sf::Color & color): CFigure(ID)
{
	SetPosition(GlobalPosition);
	triangle.setPointCount(3);
	triangle.setOrigin(sf::Vector2f((Position0.x + Position1.x + Position2.x) / 3, (Position0.y + Position1.y + Position2.y) / 3));
	triangle.setFillColor(color);
	triangle.setPosition(GlobalPosition - triangle.getOrigin());

	FirstHelper = AddFigurePoint(Position0 - triangle.getOrigin());
	SecondHelper = AddFigurePoint(Position1 - triangle.getOrigin());
	ThirdHelper = AddFigurePoint(Position2 - triangle.getOrigin());
}

void Triangle::SetColor(const sf::Color & color)
{
	triangle.setFillColor(color);
}

IFigure * Triangle::ReturnFigureCopy()
{
	return new Triangle(GetIdNumber(), GetPosition(), FirstHelper->GetRelativePosition(), SecondHelper->GetRelativePosition(), ThirdHelper->GetRelativePosition(), triangle.getFillColor());
}

bool Triangle::CheckIfMarked(const sf::Vector2f & ClickedPosition)
{
	auto globalBounds = triangle.getGlobalBounds();
	if (ClickedPosition.x >= globalBounds.left && ClickedPosition.x <= globalBounds.left + globalBounds.width && ClickedPosition.y >= globalBounds.top && ClickedPosition.y <= globalBounds.top + globalBounds.height)
		return true;
	else
		return false;
}

void Triangle::ResizeFigure(const int & delta)
{
	;//Not necessery
}

void Triangle::RotateFigure(const int& delta)
{
	RotateFigurePoints((float)delta);
}

std::string Triangle::GetDataString()
{
	return std::to_string(GetIdNumber()) + " " + std::to_string(GetPosition().x) + " " + std::to_string(GetPosition().y) + " " +
		std::to_string(FirstHelper->GetRelativePosition().x) + " " + std::to_string(FirstHelper->GetRelativePosition().y) + " " +
		std::to_string(SecondHelper->GetRelativePosition().x) + " " + std::to_string(SecondHelper->GetRelativePosition().y) + " " +
		std::to_string(ThirdHelper->GetRelativePosition().x) + " " + std::to_string(ThirdHelper->GetRelativePosition().y) + " " +
		std::to_string(triangle.getFillColor().r) + " " + std::to_string(triangle.getFillColor().g) + " " + std::to_string(triangle.getFillColor().b) + " " + std::to_string(triangle.getFillColor().a) + " ";
}

void Triangle::OnDrawFigure(sf::RenderWindow & window)
{
	triangle.setOrigin(sf::Vector2f((FirstHelper->GetRelativePosition().x + SecondHelper->GetRelativePosition().x + ThirdHelper->GetRelativePosition().x) / 3, (FirstHelper->GetRelativePosition().y + SecondHelper->GetRelativePosition().y + ThirdHelper->GetRelativePosition().y) / 3));
	triangle.setPosition(GetPosition());
	triangle.setPoint(0, FirstHelper->GetRelativePosition() + triangle.getOrigin());
	triangle.setPoint(1, SecondHelper->GetRelativePosition() + triangle.getOrigin());
	triangle.setPoint(2, ThirdHelper->GetRelativePosition() + triangle.getOrigin());

	window.draw(triangle);
}
