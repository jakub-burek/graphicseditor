#include "Utilities.h"

unsigned int IDGenerator(FigureHandler & CurrentFigureHandler, DuplicatorTool& CurrentDuplicatorTool)
{
	std::mt19937_64 generator{ std::random_device{}() };
	while (true)
	{
		const unsigned int result = std::uniform_int_distribution<unsigned int>{ 1, UINT_MAX }(generator);
		if (!CurrentFigureHandler.IdNumberExists(result) && !CurrentDuplicatorTool.IdNumberExists(result))
			return result;
	}
}

sf::Vector2f RotatePoint(sf::Vector2f center, float radians, sf::Vector2f currentPosition)
{
	float sinValue = sin(radians);
	float cosValue = cos(radians);

	currentPosition.x -= center.x;
	currentPosition.y -= center.y;

	currentPosition.x = currentPosition.x * cosValue - currentPosition.y * sinValue + center.x;
	currentPosition.y = currentPosition.x * sinValue + currentPosition.y * cosValue + center.y;
	return currentPosition;
}

sf::Vector2f CalculateVectorAB(const sf::Vector2f& A, const sf::Vector2f& B)
{
	return sf::Vector2f(B.x - A.x, B.y - A.y);
}