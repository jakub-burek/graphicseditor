#pragma once
#if !defined(__DuplicatorToolVectorElement__)
#define __DuplicatorToolVectorElement__

#include"IFigure.h"

#include<memory>
#include<vector>

class DuplicatorToolVectorElement {
public:
	DuplicatorToolVectorElement(std::shared_ptr<IFigure> DataHandlerPointer, bool Creation);
	~DuplicatorToolVectorElement() = default;

	std::shared_ptr<IFigure> GetHandleToCurrentVersion();
	IFigure* GetOldFigureCopy();
private:
	std::shared_ptr<IFigure> HandleToCurrentVersion;
	std::shared_ptr<IFigure> OldFigureCopy;
};

#endif //__DuplicatorToolVectorElement__