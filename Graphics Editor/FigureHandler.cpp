#include"FigureHandler.h"

void FigureHandler::AddFigureAndSynchroniseWithDuplicatorTool(IFigure* newFigure, DuplicatorTool& DuplicatorTool)
{
	if (newFigure != nullptr)
	{
		auto TemporaryFigurePointer = std::shared_ptr<IFigure>(newFigure);
		FigureVector.push_back(TemporaryFigurePointer);
		DuplicatorTool.GetFigureToDuplicatorToolStackAfterCreation(TemporaryFigurePointer);
	}
}

std::shared_ptr<IFigure> FigureHandler::GetFigureToClipBoard(const sf::Vector2f& Position)
{
	for (auto& element : FigureVector)
		if (element && element->CheckIfMarked(Position))
			return element;

	return nullptr;
}

void FigureHandler::AlterFigure(std::shared_ptr<IFigure> FigureHandle, IFigure* AlteringFigure)
{
	if (AlteringFigure)
	{
		for (auto& element : FigureVector)
		{
			if (element->GetIdNumber() == FigureHandle->GetIdNumber())
			{
				element = std::shared_ptr<IFigure>(AlteringFigure);
				return;
			}
		}
		FigureVector.push_back(std::shared_ptr<IFigure>(AlteringFigure));
	}
	else
		DestroyFigure(FigureHandle);
}

void FigureHandler::DestroyFigure(std::shared_ptr<IFigure> destroyedFigure)
{
	if (destroyedFigure == nullptr)
		return;
	std::vector<std::shared_ptr<IFigure>>::iterator position = std::find_if(FigureVector.begin(), FigureVector.end(), [&destroyedFigure](const std::shared_ptr<IFigure>& element) { return element->GetIdNumber() == destroyedFigure->GetIdNumber(); });
	if (position != FigureVector.end())
		FigureVector.erase(FigureVector.begin() + std::distance(FigureVector.begin(), position));
}

void FigureHandler::DrawFigures(sf::RenderWindow& window)
{
	for (const auto& element : FigureVector)
		if (element != nullptr)
			element->DrawFigure(window);
}

bool FigureHandler::IdNumberExists(unsigned int ID)
{
	for (const auto& element : FigureVector)
		if (element != nullptr && element->GetIdNumber() == ID)
			return true;
	return false;
}

void FigureHandler::ReadDataFromFile(const std::string FileName)
{
	std::ifstream InputFileStream(FileName + ".txt", std::ifstream::in);
	if (!InputFileStream.good())
		return;
	FigureVector.clear();
	std::string RetrivedData;

	try
	{
		while (getline(InputFileStream, RetrivedData))
		{
			if (RetrivedData == "class Circle")
			{
				auto TemporaryFigurePointer = AddCircleFromFile(InputFileStream);
				if (TemporaryFigurePointer == nullptr)
					return;
				else
					FigureVector.push_back(std::shared_ptr<IFigure>(TemporaryFigurePointer));
			}
			else if (RetrivedData == "class Line")
			{
				auto TemporaryFigurePointer = AddLineFromFile(InputFileStream);
				if (TemporaryFigurePointer == nullptr)
					return;
				else
					FigureVector.push_back(std::shared_ptr<IFigure>(TemporaryFigurePointer));
			}
			else if (RetrivedData == "class Quadrangle")
			{
				auto TemporaryFigurePointer = AddQuadrangleFromFile(InputFileStream);
				if (TemporaryFigurePointer == nullptr)
					return;
				else
					FigureVector.push_back(std::shared_ptr<IFigure>(TemporaryFigurePointer));
			}
			else if (RetrivedData == "class FigureGroup")
			{
				std::string data;
				if (!getline(InputFileStream, data))
					return;
			}
			else if (RetrivedData == "class Square")
			{
				auto TemporaryFigurePointer = AddSquareFromFile(InputFileStream);
				if (TemporaryFigurePointer == nullptr)
					return;
				else
					FigureVector.push_back(std::shared_ptr<IFigure>(TemporaryFigurePointer));
			}
			else if (RetrivedData == "class Triangle")
			{
				auto TemporaryFigurePointer = AddTriangleFromFile(InputFileStream);
				if (TemporaryFigurePointer == nullptr)
					return;
				else
					FigureVector.push_back(std::shared_ptr<IFigure>(TemporaryFigurePointer));
			}
			else if (RetrivedData == "class Rectangle")
			{
				auto TemporaryFigurePointer = AddRectangleFromFile(InputFileStream);
				if (TemporaryFigurePointer == nullptr)
					return;
				else
					FigureVector.push_back(std::shared_ptr<IFigure>(TemporaryFigurePointer));
			}
		}
		InputFileStream.close();
	}
	catch (const std::invalid_argument&)
	{
		InputFileStream.close();
	}
	catch (...)
	{
		InputFileStream.close();
	}
}

void FigureHandler::WriteDataToFile(const std::string FileName)
{
	std::ofstream OutputStream(FileName + ".txt", std::ofstream::out, std::ofstream::trunc);
	if (!OutputStream.good())
		return;

	for (const auto& element : FigureVector)
	{ 
		OutputStream << typeid(*element).name() << std::endl;
		OutputStream << element->GetDataString() << std::endl;
	}
	OutputStream.close();
}

void FigureHandler::ClearFigureVector()
{
	FigureVector.clear();
}

IFigure* FigureHandler::AddCircleFromFile(std::ifstream& InputFileStram)
{
	std::string data;
	unsigned int ID;
	sf::Vector2f Position;
	float radius;
	sf::Color color;

	if (getline(InputFileStram, data, ' '))
		ID = std::stoul(data);
	else
		return nullptr;

	if (!ReadVectorFromFile(InputFileStram, Position))
		return nullptr;

	if (getline(InputFileStram, data, ' '))
		radius = std::stof(data);
	else
		return nullptr;

	if (!ReadColorFromFile(InputFileStram, color))
		return nullptr;

	return new Circle(ID, Position, radius, color);
}

IFigure* FigureHandler::AddLineFromFile(std::ifstream& InputFileStram)
{
	std::string data;
	unsigned int ID;
	sf::Vector2f BeginPosition;
	sf::Vector2f EndPosition;
	sf::Color color;

	if (getline(InputFileStram, data, ' '))
		ID = std::stoul(data);
	else
		return nullptr;

	if (!ReadVectorFromFile(InputFileStram, BeginPosition))
		return nullptr;

	if (!ReadVectorFromFile(InputFileStram, EndPosition))
		return nullptr;

	if (!ReadColorFromFile(InputFileStram, color))
		return nullptr;

	return new Line(ID, BeginPosition, EndPosition, color);
}

IFigure* FigureHandler::AddQuadrangleFromFile(std::ifstream& InputFileStram)
{
	std::string data;
	unsigned int ID;
	sf::Vector2f GlobalPosition;
	sf::Vector2f Vector0;
	sf::Vector2f Vector1;
	sf::Vector2f Vector2;
	sf::Vector2f Vector3;
	sf::Color color;

	if (getline(InputFileStram, data, ' '))
		ID = std::stoul(data);
	else
		return nullptr;

	if (!ReadVectorFromFile(InputFileStram, GlobalPosition))
		return nullptr;

	if (!ReadVectorFromFile(InputFileStram, Vector0))
		return nullptr;
	if (!ReadVectorFromFile(InputFileStram, Vector1))
		return nullptr;
	if (!ReadVectorFromFile(InputFileStram, Vector2))
		return nullptr;
	if (!ReadVectorFromFile(InputFileStram, Vector3))
		return nullptr;

	if (!ReadColorFromFile(InputFileStram, color))
		return nullptr;

	return new Quadrangle(ID, GlobalPosition, Vector0, Vector1, Vector2, Vector3, color);
}

IFigure* FigureHandler::AddRectangleFromFile(std::ifstream& InputFileStram)
{
	std::string data;
	unsigned int ID;
	sf::Vector2f GlobalPosition;
	sf::Vector2f Lenght;
	sf::Color color;

	if (getline(InputFileStram, data, ' '))
		ID = std::stoul(data);
	else
		return nullptr;

	if (!ReadVectorFromFile(InputFileStram, GlobalPosition))
		return nullptr;
	if (!ReadVectorFromFile(InputFileStram, Lenght))
		return nullptr;

	if (!ReadColorFromFile(InputFileStram, color))
		return nullptr;

	return new Rectangle(ID, GlobalPosition, Lenght, color);
}

IFigure* FigureHandler::AddSquareFromFile(std::ifstream& InputFileStram)
{
	std::string data;
	unsigned int ID;
	sf::Vector2f GlobalPosition;
	float lenght;
	sf::Color color;
	float rotation;

	if (getline(InputFileStram, data, ' '))
		ID = std::stoul(data);
	else
		return nullptr;

	if (!ReadVectorFromFile(InputFileStram, GlobalPosition))
		return nullptr;

	if (getline(InputFileStram, data, ' '))
		lenght = std::stof(data);
	else
		return nullptr;

	if (!ReadColorFromFile(InputFileStram, color))
		return nullptr;

	if (getline(InputFileStram, data, ' '))
		rotation = std::stof(data);
	else
		return nullptr;

	return new Square(ID, GlobalPosition, lenght, color, rotation);
}

IFigure* FigureHandler::AddTriangleFromFile(std::ifstream& InputFileStram)
{
	std::string data;
	unsigned int ID;
	sf::Vector2f GlobalPosition;
	sf::Vector2f Vector0;
	sf::Vector2f Vector1;
	sf::Vector2f Vector2;
	sf::Color color;

	if (getline(InputFileStram, data, ' '))
		ID = std::stoul(data);
	else
		return nullptr;

	if (!ReadVectorFromFile(InputFileStram, GlobalPosition))
		return nullptr;
	if (!ReadVectorFromFile(InputFileStram, Vector0))
		return nullptr;
	if (!ReadVectorFromFile(InputFileStram, Vector1))
		return nullptr;
	if (!ReadVectorFromFile(InputFileStram, Vector2))
		return nullptr;

	if (!ReadColorFromFile(InputFileStram, color))
		return nullptr;

	return new Triangle(ID, GlobalPosition, Vector0, Vector1, Vector2, color);
}

bool FigureHandler::ReadColorFromFile(std::ifstream & InputFileStram, sf::Color& color)
{
	std::string data;

	if (getline(InputFileStram, data, ' '))
		color.r = (sf::Uint8)std::stoul(data);
	else
		return false;
	if (getline(InputFileStram, data, ' '))
		color.g = (sf::Uint8)std::stoul(data);
	else
		return false;
	if (getline(InputFileStram, data, ' '))
		color.b = (sf::Uint8)std::stoul(data);
	else
		return false;
	if (getline(InputFileStram, data, ' '))
		color.a = (sf::Uint8)std::stoul(data);
	else
		return false;

	return true;
}

bool FigureHandler::ReadVectorFromFile(std::ifstream & InputFileStream, sf::Vector2f & Vector)
{
	std::string data;

	if (getline(InputFileStream, data, ' '))
		Vector.x = std::stof(data);
	else
		return false;

	if (getline(InputFileStream, data, ' '))
		Vector.y = std::stof(data);
	else
		return false;

	return true;
}