#include "Rectangle.h"

Rectangle::Rectangle(unsigned int ID, const sf::Vector2f & Position, const sf::Vector2f & Lenght, const sf::Color & color) : CFigure(ID)
{
	SetPosition(Position);
	rectangle.setPointCount(4);
	rectangle.setPosition(Position.x - 0.5f *Lenght.x, Position.y - 0.5f *Lenght.y);
	rectangle.setOrigin(Lenght*0.5f);
	rectangle.setFillColor(color);

	leftTopHelper = AddFigurePoint(Lenght * -0.5f);
	rightBottomHelper = AddFigurePoint(Lenght * 0.5f);
}

void Rectangle::SetColor(const sf::Color & color)
{
	rectangle.setFillColor(color);
}

IFigure * Rectangle::ReturnFigureCopy()
{
	return new Rectangle(GetIdNumber(), GetPosition(), rightBottomHelper->CalculateAbsolutePosition() - leftTopHelper->CalculateAbsolutePosition(), rectangle.getFillColor());
}

bool Rectangle::CheckIfMarked(const sf::Vector2f & ClickedPosition)
{
	auto globalBounds = rectangle.getGlobalBounds();
	if (ClickedPosition.x >= globalBounds.left && ClickedPosition.x <= globalBounds.left + globalBounds.width && ClickedPosition.y >= globalBounds.top && ClickedPosition.y <= globalBounds.top + globalBounds.height)
		return true;
	else
		return false;

}

void Rectangle::ResizeFigure(const int & delta)
{
	;//Not necessery
}

void Rectangle::RotateFigure(const int & delta)
{
	RotateFigurePoints((float)delta);
}

std::string Rectangle::GetDataString()
{
	return std::to_string(GetIdNumber()) + " " + std::to_string(GetPosition().x) + " " + std::to_string(GetPosition().y) + " " +
		std::to_string(rightBottomHelper->CalculateAbsolutePosition().x - leftTopHelper->CalculateAbsolutePosition().x) + " " + std::to_string(rightBottomHelper->CalculateAbsolutePosition().y - leftTopHelper->CalculateAbsolutePosition().y) + " " +
		std::to_string(rectangle.getFillColor().r) + " " + std::to_string(rectangle.getFillColor().g) + " " + std::to_string(rectangle.getFillColor().b) + " " + std::to_string(rectangle.getFillColor().a) + " ";
}

void Rectangle::OnDrawFigure(sf::RenderWindow & window)
{
	if (leftTopHelper && rightBottomHelper)
	{
		const auto leftTopPosition = leftTopHelper->CalculateAbsolutePosition();
		const auto rightBottomPosition = rightBottomHelper->CalculateAbsolutePosition();
		const auto size = rightBottomPosition - leftTopPosition;

		rectangle.setOrigin(size*0.5f);
		rectangle.setPosition(leftTopPosition + rectangle.getOrigin());
		rectangle.setPoint(0, sf::Vector2f(0, 0));
		rectangle.setPoint(1, sf::Vector2f{ size.x, 0 });
		rectangle.setPoint(2, sf::Vector2f{ size.x, size.y });
		rectangle.setPoint(3, sf::Vector2f{ 0, size.y });
	}
	window.draw(rectangle);
}
