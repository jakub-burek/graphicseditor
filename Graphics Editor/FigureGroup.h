#pragma once
#if !defined(__Figure_Group__)
#define __Figure_Group__

#include "CFigureGroup.h"

class FigureGroup : public CFigureGroup {
public:
	FigureGroup(unsigned int ID, std::vector<std::shared_ptr<IFigure>>& SetOfFigures);

	virtual void SetColor(const sf::Color& color) override final;
	virtual IFigure* ReturnFigureCopy() override final;
	virtual bool CheckIfMarked(const sf::Vector2f& Position) override final;
	virtual void ResizeFigure(const int & delta) override final;
	virtual void RotateFigure(const int& delta) override final;
	virtual std::string GetDataString() override final;
	std::vector<std::shared_ptr<IFigure>> ReturnGroupFigureVector();
};

#endif //__Figure_Group__