# GraphicsEditor

This is simple graphics editor. It allows to draw basic shapes (color, group and delete them), change background color and save/restore image to txt file.
Programming course project, 4 semester.